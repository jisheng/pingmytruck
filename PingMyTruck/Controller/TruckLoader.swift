//
//  TruckLoader.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 05/12/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import Foundation

class TruckLoader {
    //Singleton Pattern
    static let sharedTruckLoader: TruckLoader = TruckLoader()
    private init() {}
    
    func dataFileURL() -> NSURL {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("truck.plist")
        
        return filePath
    }
    func readTruckFromFile() -> [Truck] {
        let truckData: [NSData]? = NSArray(contentsOfURL: self.dataFileURL()) as? [NSData]
        var trucks: [Truck] = []
   
        if truckData != nil {
            for data in truckData! {
                let truck: Truck = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Truck
                trucks.append(truck)
            }
        }
        return trucks
    }
    
    func saveTruckToFile(trucks: [Truck]){
        var trucksData: [NSData] = []
        for truck in trucks {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(truck)
            trucksData.append(data)
        }
        
        (trucksData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
    }
}
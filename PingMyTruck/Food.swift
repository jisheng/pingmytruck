//
//  Food.swift
//  PingMyTruck
//
//  Created by 黃善勇 on 18-11-15.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import Foundation
import UIKit

class Food : NSObject, NSCoding {

    enum Category: CustomStringConvertible {
        case Food
        case Beverage
        
        var description: String {
            get {
                switch self {
                case .Food:
                    return "Food"
                case .Beverage:
                    return "Beverage"
                }
                
            }
        }
    }
    
    var price : Double?
    var name : String {
        didSet {
            if name == "" {
                self.name = "Unnamed"
            }
        }
    }
    
    var category : Category
    var ratings : [Rating] = []
    var foodImage : UIImage?
    var menuCategory : String?
    var foodDescription : String?

    init(name: String, foodCategory: Category = .Food, initialRating: Int? = nil) {
        
        if name == "" {
            self.name = "Unnamed"
        }
        self.name = name
        self.category = foodCategory
        
        super.init()

        if let initialRating = initialRating {
            self.ratings.append(Rating(rating: initialRating))
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver

        self.init(name: keyedUnarchiver.decodeObjectForKey("PMTFoodName") as! String)
        self.price = keyedUnarchiver.decodeObjectForKey("PMTFoodPrice") as? Double
        self.menuCategory = keyedUnarchiver.decodeObjectForKey("PMTMenuCategory") as? String
        self.foodDescription = keyedUnarchiver.decodeObjectForKey("PMTFoodDescription") as? String
        
        let foodCategory = keyedUnarchiver.decodeObjectForKey("PMTFoodCategory") as? String
        
        switch foodCategory!
        {
        case "Food" :
            self.category = Food.Category.Food
        case "Beverage":
            self.category = Food.Category.Beverage
        default:
            self.category = Food.Category.Food
        }

        if let imageData = keyedUnarchiver.decodeObjectForKey("PMTFoodImage") as? NSData {
            self.foodImage = UIImage(data: imageData)
        }
        

        self.ratings = []
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.name, forKey: "PMTFoodName")
        keyedArchiver.encodeObject(self.price, forKey: "PMTFoodPrice")
        keyedArchiver.encodeObject(self.menuCategory, forKey: "PMTMenuCategory")
        keyedArchiver.encodeObject(self.foodDescription, forKey: "PMTFoodDescription")

        keyedArchiver.encodeObject(self.category.description, forKey: "PMTFoodCategory")

        if self.foodImage != nil {
            let imageData: NSData? = UIImagePNGRepresentation(self.foodImage!)
            keyedArchiver.encodeObject(imageData, forKey: "PMTFoodImage")
        }
    }
    
    func addRating(ratingValue: Int) {
        // add a new Rating object into the Food queue
        self.ratings.append(Rating(rating: ratingValue))
    }
    
    func removeRating(arrayIndex: Int?) {
        if let arrayIndex = arrayIndex {
            self.ratings.removeAtIndex(arrayIndex)
        }
    }
    
    func averageRating() -> Double {
        return Rating.average(self.ratings)
    }
}


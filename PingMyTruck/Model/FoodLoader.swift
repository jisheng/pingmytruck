////
////  FoodLoader.swift
////  PingMyTruck
////
////  Created by 黃善勇 on 5-12-15.
////  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
////
//
//import Foundation
//
//class FoodLoader {
//    
//    static let sharedLoader: FoodLoader = FoodLoader()
//    private init() {}
//    
//    // MARK: Manage data files
//    
//    let fileManager: NSFileManager = NSFileManager.defaultManager()
//    
//    func dataFileURL() -> NSURL {
//        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
//        
//        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
//        
//        return filePath
//    }
//    
//    // MARK: I/O functions
//    
//    func readFoodFromFile() -> [Food] {
//        
//        var foodInMenu: [Food] = []
//        
//        if let foodData: [NSData] = NSArray(contentsOfURL: self.dataFileURL()) as? [NSData] {
//            for data in foodData {
//                let food: Food = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Food
//                foodInMenu += [food]
//            }
//        }
//        return foodInMenu
//    }
//    
//    func saveFoodToFile(foodInMenu: [Food]) {
//        var foodData: [NSData] = []
//        
//        for food in foodInMenu {
//            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(food)
//            foodData.append(data)
//        }
//        
//        (foodData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
//    }
//
//}
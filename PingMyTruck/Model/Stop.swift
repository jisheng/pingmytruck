//
//  Stop.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 21/11/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import Foundation
import MapKit

class Stop : NSObject, NSCoding {
    var name: String
    var location: CLLocation
    var timeAtLocation: String
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyArchiver.encodeObject(self.name, forKey: "PMTStopName")
        keyArchiver.encodeObject(self.location, forKey: "PMTStopLocation")
        keyArchiver.encodeObject(self.timeAtLocation, forKey: "PMTStopTimeAtLocation")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyUnarchiver : NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let name = keyUnarchiver.decodeObjectForKey("PMTStopName") as! String
        let location = keyUnarchiver.decodeObjectForKey("PMTStopLocation") as! CLLocation
        let timeAtLocation = keyUnarchiver.decodeObjectForKey("PMTStopTimeAtLocation") as! String
        self.init(name: name, location: location, timeAtLocation: timeAtLocation)
    }
    
    
    init(name: String, location: CLLocation, timeAtLocation: String = ""){
        self.name = name
        self.location = location
        self.timeAtLocation = timeAtLocation
        super.init()
    }
}
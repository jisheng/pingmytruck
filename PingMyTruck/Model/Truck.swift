//
//  Truck.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 11/11/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Truck : NSObject, NSCoding{
    var truckName: String
    var currentStopIndex: Int
    var foodMenu: [Food]
    var foodImageURL: String
    var ratings: [Rating]
    var stops: [Stop]
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.truckName, forKey: "PMTTruckName")
        keyedArchiver.encodeObject(self.currentStopIndex, forKey: "PMTCurrentStopIndex")
        keyedArchiver.encodeObject(self.stops, forKey: "PMTstops")
        keyedArchiver.encodeObject(self.foodImageURL, forKey: "PMTFoodImageURL")
        keyedArchiver.encodeObject(self.foodMenu, forKey: "PMTFoodMenu")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let truckName = keyedUnarchiver.decodeObjectForKey("PMTTruckName") as! String
        let currentStopIndex = keyedUnarchiver.decodeObjectForKey("PMTCurrentStopIndex") as! Int
        let stops = keyedUnarchiver.decodeObjectForKey("PMTstops") as! [Stop]
        let foodImageURL = keyedUnarchiver.decodeObjectForKey("PMTFoodImageURL") as! String
        self.init(truckName: truckName, stops: stops, currentStopIndex: currentStopIndex, foodImageURL: foodImageURL)
        let foodMenu = keyedUnarchiver.decodeObjectForKey("PMTFoodMenu") as! [Food]
        self.foodMenu = foodMenu
    }
    
    init(truckName: String, stops: [Stop], currentStopIndex: Int, foodImageURL: String){
        self.truckName = truckName
        self.foodMenu = []
        self.ratings = []
        self.stops = stops
        self.currentStopIndex = currentStopIndex
        self.foodImageURL = foodImageURL
        super.init()
    }
    
    func getCurrentStop() -> Stop{
        return stops[currentStopIndex]
    }
    
    func getNearestStop(currentLocation: CLLocation) -> Stop {
        if(stops.count == 1){
            return stops[0]
        }
        else {
            var nearestStop: Stop = stops[0]
            for var i = 1; i < stops.count; i++ {
                if(stops[i].location.distanceFromLocation(currentLocation) < nearestStop.location.distanceFromLocation(currentLocation)){
                    nearestStop = stops[i]
                }
            }
            return nearestStop
        }
    }
    
    func addRating(newRating: Rating){
        self.ratings.append(newRating)
    }
    
    func getAverageRatings() -> Double {
        return Rating.average(self.ratings)
    }
    
    func addFood(newFood: Food){
        self.foodMenu.append(newFood)
    }
}
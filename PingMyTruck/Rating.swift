//
//  Rating.swift
//  PingMyTruck
//
//  Created by 黃善勇 on 18-11-15.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import Foundation

class Rating : NSObject, NSCoding {

    var dateCreated : NSDate
    var userName: String?

    var rating : Int {
        didSet {
            rating = min(max(rating,1),5)
        }
    }
    
    init(rating: Int, date: NSDate = NSDate()) {
        self.rating = min(max(rating,1),5)
        self.dateCreated = date
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        self.init(rating: keyedUnarchiver.decodeObjectForKey("RatingValue") as! Int,
            date: keyedUnarchiver.decodeObjectForKey("RatingDate") as! NSDate)
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.rating, forKey: "RatingValue")
        keyedArchiver.encodeObject(self.dateCreated, forKey: "RatingDate")
        
    }
    
    class func average(ratings: [Rating]) -> Double {
        if (ratings.count < 1){
            return 0.0
        }
        
        var sum: Int = 0
        let count: Int = ratings.count
        for var i = 0; i < count; i++ {
            sum += ratings[i].rating
        }
        return round ( Double(sum) * 10 / Double(count) ) / 10.0
    }
    
    
    
}


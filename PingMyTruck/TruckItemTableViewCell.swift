//
//  TruckItemTableViewCell.swift
//  PingMyTruck
//
//  Created by 黃善勇 on 9-11-15.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import UIKit
import MapKit

class TruckItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var truckNameLabel: UILabel!
    @IBOutlet weak var stopAtLabel: UILabel!
    @IBOutlet weak var distanceToUserLabel: UILabel!
    @IBOutlet weak var truckPictureImageView: UIImageView!
    
    var truck: Truck! {
        didSet{
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: Functions
    
    func updateUI()
    {
        let myLocation = CLLocation(latitude: 0, longitude: 0)
        truckNameLabel.text = truck.truckName
        stopAtLabel.text = truck.getCurrentStop().name
        distanceToUserLabel.text = "\(round(truck.getCurrentStop().location.distanceFromLocation(myLocation)*10)/10) km"
        let urlString : String = truck.foodImageURL
        let url = NSURL(string: urlString)!
        let data = NSData(contentsOfURL: url) //make sure your image in this url does exist, otherwise unwrap in a if let check
        truckPictureImageView.image = UIImage(data: data!)
    }

}

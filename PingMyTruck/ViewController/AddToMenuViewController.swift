//
//  AddToMenuViewController.swift
//  PingMyTruck
//
//  Created by 黃善勇 on 2-12-15.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import UIKit

protocol AddToMenuDelegate {
    
    func addToMenuDelegate(ViewController: AddToMenuViewController, didAddFood food: Food)
}

class AddToMenuViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    @IBOutlet weak var foodNameTextField: UITextField!
    
    @IBOutlet weak var priceTextField: UITextField!
    
    @IBOutlet weak var foodImageView: UIImageView!
    
    @IBOutlet weak var foodDescriptionTextField: UITextField!
    
    var food : Food?
    var delegate: AddToMenuDelegate?
    
    var foodAddedBlock : ((food : Food!) -> Void)?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        foodNameTextField.becomeFirstResponder()
        
    }
    
    // MARK: Navigation
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    // MARK: UITextFieldDelegate
    
    
    // MARK: Action Methods
    
    @IBAction func selectImageFromPhotoLibrary(sender: UITapGestureRecognizer) {
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.sourceType = .PhotoLibrary
        
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    
    @IBAction func saveFood(sender: AnyObject) {
        self.food = Food(name: foodNameTextField.text!)

        if let price = priceTextField.text {
            // need to check valid number
            self.food!.price = Double(price)
        }
        
//        self.delegate?.addToMenuDelegate(self, didAddFood: self.food!)
//        self.food!.foodImage = UIImage(named: foodNameTextField.text!)

        self.food!.foodImage = foodImageView.image
        
        self.food!.foodDescription = foodDescriptionTextField.text
        
        self.foodAddedBlock?(food: self.food!)
        
        dismissViewControllerAnimated(true, completion: nil)
    
    }
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        foodImageView.image = selectedImage
        
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
}

//
//  addTruckVC.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 07/11/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import UIKit
import MapKit

protocol AddTruckDelegate {
    func viewController(vc: AddTruckViewController, didAddTruck truck: Truck!)
}

class AddTruckViewController: UIViewController {

    @IBOutlet weak var TruckNameTextField: UITextField!
    var truck : Truck?
    var delegate: AddTruckDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Cancel(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func AddTruck(sender: AnyObject) {
        let stopsName:[String] = ["Cyberjaya","Putrajaya","KLCC","IOI Mall","Menara A & B"]
        let foodImageUrls: [String] = ["https://system.qtixapp.com/images/prettyphoto/1.jpg","https://system.qtixapp.com/images/prettyphoto/2.jpg","https://system.qtixapp.com/images/prettyphoto/3.jpg","https://system.qtixapp.com/images/prettyphoto/4.jpg","https://www.usedvending.com/images/food-trucks-mobile-kitchens-for-sale/antique-food-truck-milk.jpg","https://bakerytrucksforsale.com/images/uploads/ford_walkin_baking_snack_truck_1.jpg"
        ]
        
        truck = Truck(
            truckName: TruckNameTextField.text!,
            stops: [
                Stop(
                    name: stopsName[random() % stopsName.count],
                    location: CLLocation(
                        latitude: Double(random() % 600 + 1) / 1000000.0 ,
                        longitude: Double(random() % 600 + 1) / 1000000.0
                    )
                )
            ],
            currentStopIndex: 0,
            foodImageURL: foodImageUrls[random() % foodImageUrls.count]
        )
        
        self.delegate?.viewController(self, didAddTruck: self.truck!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
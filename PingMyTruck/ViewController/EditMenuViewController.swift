//
//  editMenuVC.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 07/11/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import UIKit

protocol AddFoodDelegate {
    func addFoodDelegate(ViewController: EditMenuViewController, didAddFood: Food)
}

class EditMenuViewController: UIViewController {

    var food : Food!
    
    @IBOutlet weak var foodNameTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var foodTypePicker: UIPickerView!

    @IBOutlet weak var foodPriceTextField: UITextField!
    @IBOutlet weak var foodItemImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateUI()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func updateUI() {
        
        
        foodNameTextField.text = food.name
        descriptionTextField.text = food.foodDescription
        
        if let price = food.price {
            foodPriceTextField.text = "\(price)"
            
        } else {
            foodPriceTextField.text = "0.00"
        }
        
        foodItemImageView.image = food.foodImage
        
        //   foodItemImageView.contentMode = // how to set the content size
        
    }

    @IBAction func saveData(sender: AnyObject) {
        
        self.food.name = self.foodNameTextField.text!
        self.food.price = Double(self.foodPriceTextField.text!)
        self.food.foodDescription = self.descriptionTextField.text
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
}

extension EditMenuViewController : UITextFieldDelegate {

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == foodNameTextField {
            self.descriptionTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}
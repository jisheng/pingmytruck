//
//  MenuItemTableViewCell.swift
//  PingMyTruck
//
//  Created by 黃善勇 on 5-12-15.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var foodItemImageVIew: UIImageView!
    @IBOutlet weak var foodNameLabel: UILabel!
    @IBOutlet weak var foodDescriptionTextField: UITextField!

    var foodItem : Food? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: Methods
    
    func updateUI() {
        
        self.foodNameLabel.text = foodItem?.name
        //        self.foodItemImageVIew.image = foodItem?.foodImage
        
        if let foodName = foodItem?.name {
            self.foodItemImageVIew.image = UIImage(named: foodName)
        }
        if let foodImage = foodItem?.foodImage {
            self.foodItemImageVIew.image = foodImage
        }
        
        let textToShow = "(\(foodItem?.price ?? 0.00))" + " \(foodItem?.foodDescription ?? "")"

        self.foodDescriptionTextField.text = textToShow
    }
    
}

//
//  MenuViewController.swift
//  PingMyTruck
//
//  Created by Wong Shan Yung on 07/11/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var menuItemTableView: UITableView!
    @IBOutlet weak var truckNameLabel: UILabel!
    
    @IBOutlet weak var truckDescLabel: UILabel!
    @IBOutlet weak var menuCategoryLabel: UILabel!
    @IBOutlet weak var truckImage: UIImageView!
    @IBOutlet weak var sortButton: UIButton!
    
    var foodInMenu : [Food] = []
    var sortedFood : [Food] = []
    var truck: Truck!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuItemTableView.estimatedRowHeight = menuItemTableView.rowHeight
        menuItemTableView.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
        updateDetails()
        populateData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        if !foodInMenu.isEmpty {
//            FoodLoader.sharedLoader.saveFoodToFile(self.foodInMenu)
        }
        self.menuItemTableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "AddFoodSegue" {
            let destVC = (segue.destinationViewController as! UINavigationController).topViewController as? AddToMenuViewController

            if destVC != nil {
                
                destVC?.foodAddedBlock = {(food: Food!) -> Void in
                    self.foodInMenu += [food]
                    self.truck.foodMenu += [food]

                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                    // sort the list again before presenting on table
                    self.sortedFood = self.foodInMenu.sort( {
                        (f1: Food, f2:Food) -> Bool in
                        return f1.name < f2.name
                    })
                    
                    self.menuItemTableView.reloadData()
                    
                }
                
            }
        }
        
        
        if segue.identifier == "EditFoodSegue" {
            
            // Touching on the item itself
            
            let destVC = (segue.destinationViewController as! UINavigationController).topViewController as? AddToMenuViewController
            
            if destVC != nil {
                
                if let selectedFoodCell = sender as? MenuItemTableViewCell {
                    let indexPath = menuItemTableView.indexPathForCell(selectedFoodCell)!
                    
                    let selectedFood = foodInMenu[indexPath.row]
                    
                    destVC!.food = selectedFood
                    
                }
            }
            
        }
        
//        if segue.identifier == "AddToMenuSegue" {
//            let destVC : AddToMenuViewController = segue.destinationViewController as! AddToMenuViewController
//            
//            destVC.delegate = self
//            
//            destVC.foodAddedBlock = {(food: Food!) -> Void in
//                self.foodInMenu += [food]
//                self.truck.foodMenu += [food]
//                
//                //FoodLoader.sharedLoader.saveFoodToFile(self.foodInMenu)
//
//                self.dismissViewControllerAnimated(true, completion: nil)
//                
//                // sort the list again before presenting on table
//                self.sortedFood = self.foodInMenu.sort( {
//                    (f1: Food, f2:Food) -> Bool in
//                    return f1.name < f2.name
//                })
//
//                self.menuItemTableView.reloadData()
//                
//            }
//        
//        }

    }
    
    
    // MARK: - Methods
    
    func updateDetails()
    {
        truckNameLabel.text = truck.truckName
        truckDescLabel.text = truck.getCurrentStop().name
        let urlString : String = truck.foodImageURL
        let url = NSURL(string: urlString)!
        let data = NSData(contentsOfURL: url) //make sure your image in this url does exist, otherwise unwrap in a if let check
        truckImage.image = UIImage(data: data!)
        menuCategoryLabel.text = "Hot food"
        self.foodInMenu = truck.foodMenu
    
    }
    
    func populateData() {

    /*
//        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        // reading data

//        let readFoodData: NSData = defaults.objectForKey("testFood") as! NSData
//        let readFood: Food? = NSKeyedUnarchiver.unarchiveObjectWithData(readFoodData) as? Food

    
        

        // read list of food items from NSUserDefaults
        
        //self.foodInMenu = FoodLoader.sharedLoader.readFoodFromFile()

//        if let foodDataList : [NSData] = defaults.objectForKey("FoodItemsInMenu") as? [NSData] {
//            for foodData in foodDataList {
//                foodInMenu.append((NSKeyedUnarchiver.unarchiveObjectWithData(foodData) as? Food)!)
//            }
//        } else
        
//        if foodInMenu.isEmpty {
//            // if there's nothing in the list, we pre-populate something here
//            
//            // Food
//            // foodInMenu.append(Food(name: "Nasi Lemak"))
//            foodInMenu += [Food(name: "Nasi Lemak")]
//            foodInMenu.last?.foodDescription = "Malaysians' all time favorite breakfast"
//            foodInMenu.last?.price = 2.90
//            foodInMenu.last?.foodImage = UIImage(named: (foodInMenu.last?.name)!)
//            
//            
//            foodInMenu.append(Food(name: "Lekor"))
//            foodInMenu.last?.foodDescription = "Malaysians' everyday snack"
//            foodInMenu.last?.price = 0.25
//            foodInMenu.last?.foodImage = UIImage(named: (foodInMenu.last?.name)!)
//            
//            foodInMenu.append(Food(name: "虾面",foodCategory: Food.Category.Food))
//            foodInMenu.last?.foodDescription = "Penang soup noodle"
//            foodInMenu.last?.price = 5.50
//            foodInMenu.last?.foodImage = UIImage(named: (foodInMenu.last?.name)!)
//            
//            
//            foodInMenu.append(Food(name: "Roti Bakar",foodCategory: Food.Category.Food))
//            foodInMenu.last?.foodDescription = "Topped with butter"
//            foodInMenu.last?.price = 1.50
//            foodInMenu.last?.foodImage = UIImage(named: (foodInMenu.last?.name)!)
//            
//            
//            // Beverages
//            foodInMenu.append(Food(name: "Milo",foodCategory: Food.Category.Beverage))
//            foodInMenu.last?.foodDescription = "Steamy hot"
//            foodInMenu.last?.price = 2.50
//            foodInMenu.last?.foodImage = UIImage(named: (foodInMenu.last?.name)!)
//            
//            foodInMenu.append(Food(name: "Flat White",foodCategory: Food.Category.Beverage))
//            foodInMenu.last?.foodDescription = "Aromatic Vietnamese bean"
//            foodInMenu.last?.price = 9.00
//            foodInMenu.last?.foodImage = UIImage(named: (foodInMenu.last?.name)!)
//            
//            
//            foodInMenu.append(Food(name: "Teh Tarik",foodCategory: Food.Category.Beverage))
//            foodInMenu.last?.foodDescription = "Foamy hot and sweet"
//            foodInMenu.last?.price = 2.00
//            foodInMenu.last?.foodImage = UIImage(named: (foodInMenu.last?.name)!)
//        }
*/

        sortedFood = foodInMenu.sort( {
            (f1: Food, f2:Food) -> Bool in
            return f1.name < f2.name
        })
        
    }

    
    @IBAction func saveData() {

        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()

        // saving food
        var foodDataList : [NSData] = []
        
        for food in foodInMenu {
            foodDataList.append(NSKeyedArchiver.archivedDataWithRootObject(food))
        }
        
        defaults.setObject(foodDataList, forKey: "FoodItemsInMenu")
        defaults.synchronize()
        
    }
    
    @IBAction func sortTableList(sender: AnyObject) {
        self.menuItemTableView.reloadData()


        
        if sortButton.titleLabel!.text!.hasSuffix("type") {
            sortedFood = foodInMenu.sort( {
                (f1: Food, f2:Food) -> Bool in
                return f1.category.description < f2.category.description
            })
            sortButton.titleLabel!.text = "Sort by name"
        } else {
            sortedFood = foodInMenu.sort( {
                (f1: Food, f2:Food) -> Bool in
                return f1.name < f2.name
            })
            sortButton.titleLabel!.text = "Sort by type"
        }
       
        self.menuItemTableView.reloadData()

        
    }
    
    @IBAction func addNewFood(sender: AnyObject) {
        let edMenuVC: EditMenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("editMenuVC") as! EditMenuViewController
        
        let food: Food = Food(name: "")
        edMenuVC.food = food
        
        self.presentViewController(edMenuVC, animated: true, completion: nil)

    }
    
}

// MARK: - Extension

extension MenuViewController : AddToMenuDelegate {
    
    func addToMenuDelegate(ViewController: AddToMenuViewController, didAddFood food: Food) {
        
        self.foodInMenu += [food]
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        sortedFood = foodInMenu.sort( {
            (f1: Food, f2:Food) -> Bool in
            return f1.name < f2.name
        })
        
        self.menuItemTableView.reloadData()
        

    }
    

}

// MARK: UITableViewDataSource

extension MenuViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell : UITableViewCell  = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "FoodItemCell")
        
        let cell : MenuItemTableViewCell = tableView.dequeueReusableCellWithIdentifier("FoodItemCell", forIndexPath: indexPath) as! MenuItemTableViewCell
        
       
        cell.foodItem = sortedFood[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodInMenu.count
        
    }
    
}

extension MenuViewController : UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let edMenuVC: EditMenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("editMenuVC") as! EditMenuViewController

        let food: Food = self.sortedFood[indexPath.row]
        edMenuVC.food = food

        self.presentViewController(edMenuVC, animated: true, completion: nil)

    }
    
    
}
//
//  truckListVC.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 07/11/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import UIKit
import MapKit

class TruckListViewController: UIViewController {
    var trucks: [Truck] = []
    var sortedTrucks = [Truck]()
    
    @IBOutlet weak var trucksTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TruckListViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //        return trucks[section].count
        return trucks.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 88
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let myLocation = CLLocation(latitude: 0, longitude: 0)
        let cell = tableView.dequeueReusableCellWithIdentifier("Truck", forIndexPath: indexPath) as! TruckItemTableViewCell
        sortedTrucks = trucks.sort{
            (t1: Truck, t2: Truck) -> Bool in
            return t1.getCurrentStop().location.distanceFromLocation(myLocation) < t2.getCurrentStop().location.distanceFromLocation(myLocation)
        }
        // Configure the cell...
        
        cell.truck = sortedTrucks[indexPath.row]
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAddTruckViewController" {
            let destNavVC: UINavigationController = segue.destinationViewController as! UINavigationController
            let destVC: AddTruckViewController = destNavVC.viewControllers.first as! AddTruckViewController
            destVC.delegate = self
        }
    }
}

extension TruckListViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let menuStoryboard : UIStoryboard = UIStoryboard(name: "menu", bundle: nil)
        let menuVC : MenuViewController = menuStoryboard.instantiateViewControllerWithIdentifier("menuViewVC") as! MenuViewController
        menuVC.truck = self.sortedTrucks[indexPath.row]
        self.navigationController?.pushViewController(menuVC, animated: true)
        
    }
}

extension TruckListViewController : AddTruckDelegate {
    func viewController(vc: AddTruckViewController, didAddTruck truck: Truck!) {
        self.trucks.append(truck)
        self.dismissViewControllerAnimated(true, completion: nil)
        self.trucksTableView.reloadData()
    }
}
//
//  FoodClassTests.swift
//  PingMyTruck
//
//  Created by 黃善勇 on 21-11-15.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import XCTest
@testable import PingMyTruck


class FoodClassTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testFoodInit() {
        let cendol = Food(name: "Cendol")
        
        let cendolWithCategory = Food(name: "Cendol", foodCategory: Food.Category.Food)
        
        
        let cendolWithCategoryAndRating = Food(name: "Cendol", foodCategory: Food.Category.Food, initialRating: 1)
        
        let cendolWithoutCategoryWithRating = Food(name: "Cendol", initialRating: 4)
        
        
        XCTAssertTrue(cendol.name == "Cendol")
        XCTAssertTrue(cendolWithCategory.category == Food.Category.Food)
        XCTAssertTrue(cendolWithCategoryAndRating.ratings.count != 0)

        XCTAssertTrue(cendolWithoutCategoryWithRating.category == Food.Category.Food)
        XCTAssertTrue(cendolWithoutCategoryWithRating.ratings.count != 0)
        
        
        // test Category description
        XCTAssertTrue("\(cendolWithCategory.category)" == "Food")

        
        let variableNameTruck = Food(name: "")
        
        XCTAssertEqual(variableNameTruck.name, "", "When initializing with empty string, the truck should be called Unnamed")

        
        // when assigned an empty string, default the name
        variableNameTruck.name = ""
        
        XCTAssertTrue(variableNameTruck.name == "Unnamed")

        // otherwise, keep the name
        variableNameTruck.name = "Look Look"
        
        XCTAssertTrue(variableNameTruck.name == "Look Look")
        
        
        let myFood = Food(name: "Cendol")
        
        var myFoodList : [Food] = []
        
        XCTAssertNotNil(myFood)
        
        // test category default is .Food
        XCTAssertEqual(myFood.category, Food.Category.Food)
        
        // add some food with category
        myFoodList.append(Food(name: "Latte",foodCategory: Food.Category.Beverage))
        
        XCTAssertEqual(myFoodList.last!.category, Food.Category.Beverage)
        
    }
    
    func testFoodArraySort() {
        var foodMenu : [Food] = []
        
        foodMenu.append(Food(name: "Nasi Lemak"))
        foodMenu.append(Food(name: "Chicken Rice"))
        foodMenu.append(Food(name: "Nasi Kandar"))
        foodMenu.append(Food(name: "Cappucino",foodCategory: Food.Category.Beverage))
        foodMenu.append(Food(name: "Flat White",foodCategory: Food.Category.Beverage))
        foodMenu.append(Food(name: "Long Black",foodCategory: Food.Category.Beverage))
        
        foodMenu.append(Food(name: "Mamak Rojak"))
        foodMenu.append(Food(name: "Sarawak Rojak"))
        foodMenu.append(Food(name: "Penang Rojak"))
        foodMenu.append(Food(name: "Johor Rojak"))

        
        
        
        let sortedMenu = foodMenu.sort( {
            (f1: Food, f2: Food) -> Bool in
            return f1.name < f2.name
        })
        
        let reversedSortedMenu = foodMenu.sort( {
            (s1: Food, s2: Food) -> Bool in
            return s1.name > s2.name
        })
        
        XCTAssertTrue(sortedMenu.first!.name == "Cappucino")
        
        XCTAssertTrue("\(sortedMenu.first!.category)" == "Beverage")

        XCTAssertTrue(sortedMenu.last!.name == "Sarawak Rojak")
        
        XCTAssertTrue(reversedSortedMenu.first!.name == "Sarawak Rojak")
        XCTAssertTrue(reversedSortedMenu.last!.name == "Cappucino")
        
        
    }
    
    
    func testFoodRatingFunctions() {
        var foodMenu : [Food] = []
        
        // index 0
        foodMenu.append(Food(name: "Roti Canai Pisang",foodCategory: Food.Category.Food, initialRating: 3))
        
        // index 1
        foodMenu.append(Food(name: "Roti Canai Telur",foodCategory: Food.Category.Food, initialRating: 2))

        // index 2
        foodMenu.append(Food(name: "Strawberry pancake"))
        
        // No rating on the food
        XCTAssertTrue(foodMenu[2].averageRating() == 0)
        
        
        XCTAssertTrue(foodMenu.first!.ratings.count != 0)
        
        foodMenu[0].addRating(4)
        foodMenu[0].addRating(2)
        foodMenu[0].addRating(1)

        let valueToTest = 2.5  // (3+4+2+1)/(foodMenu[0].ratings!.count)

        XCTAssertEqual(foodMenu[0].averageRating(), valueToTest)
        
        foodMenu[0].ratings[0].rating = 10
        
        XCTAssertTrue(foodMenu[0].ratings[0].rating == 5)
        

        foodMenu[0].ratings[1].rating = 0
        
        XCTAssertTrue(foodMenu[0].ratings[1].rating == 1)

        
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}

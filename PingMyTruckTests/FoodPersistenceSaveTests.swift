////
////  FoodPersistenceSaveTests.swift
////  PingMyTruck
////
////  Created by 黃善勇 on 2-12-15.
////  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
////
//
//import XCTest
//@testable import PingMyTruck
//
//
//class FoodPersistenceSaveTests: XCTestCase {
//
//    override func setUp() {
//        super.setUp()
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//    
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        super.tearDown()
//    }
//
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measureBlock {
//            // Put the code you want to measure the time of here.
//        }
//    }
//    
//    func testWritingtoFile() {
//        let fileMgr : NSFileManager = NSFileManager.defaultManager()
//        let documentDirectory : NSURL = fileMgr.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
//        let filePath : NSURL = documentDirectory.URLByAppendingPathComponent("data.plist")
//        
//        var foodInMenu : [Food] = []
//        
//        foodInMenu += [Food(name: "Nasi")]
//        foodInMenu += [Food(name: "Noodle")]
//        
//        var foodInMenuData : [NSData] = []
//        
//        for food in foodInMenu {
//            let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(food)
//            foodInMenuData.append(data)
//        }
//        
//        (foodInMenuData as NSArray).writeToURL(filePath, atomically: true)
//        
//        let fileExists: Bool = fileMgr.fileExistsAtPath(filePath.path!)
//        
//        XCTAssert(fileExists, "File should exist after write")
//        
//        NSLog("file path: \(filePath.path!)")
//
//    }
//    
//    func testReadingFromFile() {
//        let fileMgr : NSFileManager = NSFileManager.defaultManager()
//        let documentDirectory : NSURL = fileMgr.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
//        let filePath : NSURL = documentDirectory.URLByAppendingPathComponent("data.plist")
//        
//        var foodInMenu : [Food] = []
//        
//        let foodInMenuData : [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
//        
//        for data in foodInMenuData {
//            let food: Food = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Food
//            foodInMenu.append(food)
//        }
//        
//        XCTAssert(foodInMenu.count == 2, "Should have loaded 2 items from file")
//    }
//    
//    func testSavingFood() {
//        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
//        
//        let food : Food = Food(name: "Nasi Lemak")
//        let foodData: NSData = NSKeyedArchiver.archivedDataWithRootObject(food)
//        
//        defaults.setObject(foodData, forKey: "testFood")
//        defaults.synchronize()
//        
//        let readFoodData: NSData = defaults.objectForKey("testFood") as! NSData
//        let readFood: Food? = NSKeyedUnarchiver.unarchiveObjectWithData(readFoodData) as? Food
//        XCTAssertNotNil(readFood, "Read back from saved data")
//        XCTAssertEqual(readFood!.name, food.name, "Names should match")
//
//        
//    }
//
//}

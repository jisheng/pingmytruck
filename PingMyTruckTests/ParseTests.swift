//
//  ParseTests.swift
//  PingMyTruck
//
//  Created by 黃善勇 on 9-12-15.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import XCTest
@testable import PingMyTruck

class ParseTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testLoadingURL() {
        let expectation = self.expectationWithDescription("Load Task") // wait for the response

        let session: NSURLSession = NSURLSession.sharedSession()
        
        let url: NSURL = NSURL(string: "http://apple.com")!
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            let htmlString : String = String(data: data!, encoding: NSUTF8StringEncoding)!
            
            XCTAssert(!htmlString.isEmpty, "Should have received some html STring from server")
            
            NSLog("Task completed")
            expectation.fulfill() // to test that the expectation
        }
        
        loadTask.resume()
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testLoadingAPI() {
        let expectation = self.expectationWithDescription("Load Task")
        
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["X-Parse-Application-Id": "iutePtr8XOYO1iGAkUMBsaV0CIKEqnrNpZASSLyN",
            "X-Parse-REST-API-Key": "8MAj3JPwnSF4T9Wue95QDfht8ExqjLJtvvdeRtbe",
            "Content-Type": "application/json"]
        
        let session: NSURLSession = NSURLSession(configuration: config)
        
        let url: NSURL = NSURL(string: "https://api.parse.com/1/classes/Food")!
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Server should have returned 200 Content OK")
            expectation.fulfill()
        }
        
        loadTask.resume()
        self.waitForExpectationsWithTimeout(10, handler: nil)

    }
    
    func testLoadingJSONFromAPI() {
        let expectation = self.expectationWithDescription("Load Task")
        
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["X-Parse-Application-Id": "iutePtr8XOYO1iGAkUMBsaV0CIKEqnrNpZASSLyN",
            "X-Parse-REST-API-Key": "8MAj3JPwnSF4T9Wue95QDfht8ExqjLJtvvdeRtbe",
            "Content-Type": "application/json"]
        
        let session: NSURLSession = NSURLSession(configuration: config)
        
        let url: NSURL = NSURL(string: "https://api.parse.com/1/classes/Food")!
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode

            let jsonString : String = String(data: data!, encoding: NSUTF8StringEncoding)!
            
            var meals : [Food] = []
            
            let json : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            
            if let resultsArray : [[String: AnyObject] ] = json["results"] as? [[String: AnyObject]] {
                for jsonData in resultsArray {
                    let name: String = jsonData["name"] as! String
                    let meal: Food = Food(name: name)
                    
                    meals.append(meal)
                }
            }
            
            XCTAssert(httpCode == 200, "Server should have returned 200 Content OK")
            
            expectation.fulfill()

        }
        
        loadTask.resume()
        self.waitForExpectationsWithTimeout(10, handler: nil)

    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}

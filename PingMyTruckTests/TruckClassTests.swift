////
////  TruckClassTests.swift
////  PingMyTruck
////
////  Created by Tan Ji Sheng on 21/11/2015.
////  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
////
//
//import XCTest
//@testable import PingMyTruck
//import UIKit
//import MapKit
//
//class TruckClassTests: XCTestCase {
//
//    override func setUp() {
//        super.setUp()
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//    
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        super.tearDown()
//    }
//    
//    
//    func testNewStopList(){
//        var stops: [Stop] = []
//        stops.append(Stop(name: "MaGIC", location: CLLocation(latitude: 1, longitude: 1)))
//        stops.append(Stop(name: "Cyberjaya", location: CLLocation(latitude: 5, longitude: 5)))
//        stops.append(Stop(name: "Putrajaya", location: CLLocation(latitude: 3, longitude: 3)))
//        XCTAssertTrue(stops.count == 3, "Adding Stop into list failed")
//    }
//    
//    func testNewTruck(){
//        var stops: [Stop] = []
//        let currentStop: Stop = Stop(name: "Cyberjaya", location: CLLocation(latitude: 5, longitude: 5))
//        stops.append(Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1)))
//        stops.append(currentStop)
//        stops.append(Stop(name: "Putrajaya", location: CLLocation(latitude: 3, longitude: 3)))
//        let newTruck: Truck = Truck(truckName: "CendolTruck", stops: stops, currentStopIndex: 1)
//        XCTAssertTrue(newTruck.truckName == "CendolTruck" && newTruck.getCurrentStop().name == currentStop.name)
//    }
//    
//    func testNewTruckList(){
//        var trucks: [Truck] = []
//        trucks.append(Truck(truckName: "CendolTruck", stops: [Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "CendolTruck", stops: [Stop(name: "Cyberjaya",location: CLLocation(latitude: 5, longitude: 5))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "CendolTruck", stops: [Stop(name: "Putrajaya",location: CLLocation(latitude: 3, longitude: 3))], currentStopIndex: 0))
//        XCTAssertTrue(trucks.count == 3)
//    }
//    
//    func testSortTruckListByName(){
//        var trucks: [Truck] = []
//        trucks.append(Truck(truckName: "KeropokTruck", stops: [Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "RojakTruck", stops: [Stop(name: "Cyberjaya",location: CLLocation(latitude: 5, longitude: 5))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "CendolTruck", stops: [Stop(name: "Putrajaya",location: CLLocation(latitude: 3, longitude: 3))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "DurianCendolTruck", stops: [Stop(name: "Putrajaya",location: CLLocation(latitude: 3, longitude: 3))], currentStopIndex: 0))
//        
//        
//        let sortedTrucks = trucks.sort{
//            (t1: Truck, t2: Truck) -> Bool in
//            return t1.truckName < t2.truckName
//        }
//        XCTAssertTrue(sortedTrucks[0].truckName == "CendolTruck")
//        XCTAssertTrue(sortedTrucks[3].truckName == "RojakTruck")
//    }
//    
//    func testSortTruckListByDistance(){
//        var trucks: [Truck] = []
//        trucks.append(Truck(truckName: "KeropokTruck", stops: [Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "RojakTruck", stops: [Stop(name: "Cyberjaya",location: CLLocation(latitude: 5, longitude: 5))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "CendolTruck", stops: [Stop(name: "Putrajaya",location: CLLocation(latitude: 3, longitude: 3))], currentStopIndex: 0))
//        trucks.append(Truck(truckName: "DurianCendolTruck", stops: [Stop(name: "Putrajaya",location: CLLocation(latitude: 2, longitude: 2))], currentStopIndex: 0))
//        
//        let myCurrentLocation = CLLocation(latitude: 0, longitude: 0)
//        let sortedTrucks = trucks.sort{
//            (t1: Truck, t2: Truck) -> Bool in
//            return t1.getCurrentStop().location.distanceFromLocation(myCurrentLocation) < t2.getCurrentStop().location.distanceFromLocation(myCurrentLocation)
//        }
//        XCTAssertTrue(sortedTrucks[0].truckName == "KeropokTruck")
//        XCTAssertTrue(sortedTrucks[3].truckName == "RojakTruck")
//    }
//    
//    func testAverageRatings(){
//        let truck = Truck(truckName: "KeropokTruck", stops: [Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1))], currentStopIndex: 0)
//        truck.addRating(Rating(rating: 5))
//        truck.addRating(Rating(rating: -1))
//        truck.addRating(Rating(rating: 4))
//        truck.addRating(Rating(rating: 2))
//        truck.addRating(Rating(rating: 3))
//        truck.addRating(Rating(rating: 999))
//        truck.addRating(Rating(rating: 2))
//        XCTAssertTrue(truck.getAverageRatings() == 3.1, "The rating should be capped between 1 and 5")
//    }
//    
//    func testUpdateTruck(){
//        let truck = Truck(truckName: "KeropokTruck", stops: [Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1))], currentStopIndex: 0)
//        truck.truckName = "CendolTruck"
//        XCTAssertTrue(truck.truckName=="CendolTruck", "A truck name must be able to update")
//    }
//    
//    func testAddingFood(){
//        let truck = Truck(truckName: "KeropokTruck", stops: [Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1))], currentStopIndex: 0)
//        truck.foodMenu.append(Food(name: "Keropok Lekor"))
//        truck.foodMenu.append(Food(name: "Keropok Nipis"))
//        truck.foodMenu.append(Food(name: "Keropok Pisang"))
//        XCTAssertTrue(truck.foodMenu[0].name == "Keropok Lekor")
//        XCTAssertTrue(truck.foodMenu[1].name == "Keropok Nipis")
//        XCTAssertTrue(truck.foodMenu[2].name == "Keropok Pisang")
//    }
//    
//
//
//}

//
//  TruckParseGetTest.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 09/12/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import XCTest
@testable import PingMyTruck
import MapKit

class TruckParseGetTest: XCTestCase {

    var session: NSURLSession!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["X-Parse-Application-Id": "cebNfOg7nCyzhpe2feztlfU8VuXorp751uQTN9cL",
            "X-Parse-REST-API-Key": "FO340Ne889wD0R4ZTI44sLPEkgxYmxFv8cQkUWVq",
            "Content-Type": "application/json"]
        
        self.session = NSURLSession(configuration: config)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoadingAPI() {
        let expectation = self.expectationWithDescription("Load Task")
        
        let url: NSURL = NSURL(string: "https://api.parse.com/1/classes/Truck")!
        
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
//            let htmlString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
//            XCTAssert(htmlString != nil, "Should have received some html String from server")
            
            
            //NSLog(htmlString!)
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Server should have returned 200 Content OK")
            
            
            var trucks = [Truck]()
            let stopsName:[String] = ["Cyberjaya","Putrajaya","KLCC","IOI Mall","Menara A & B"]
            let foodImageUrls: [String] = ["https://system.qtixapp.com/images/prettyphoto/1.jpg","https://system.qtixapp.com/images/prettyphoto/2.jpg","https://system.qtixapp.com/images/prettyphoto/3.jpg","https://system.qtixapp.com/images/prettyphoto/4.jpg","https://www.usedvending.com/images/food-trucks-mobile-kitchens-for-sale/antique-food-truck-milk.jpg","https://bakerytrucksforsale.com/images/uploads/ford_walkin_baking_snack_truck_1.jpg"
            ]
            
            let json : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            
            if let resultsArray: [ [String : AnyObject] ] = json["results"] as? [ [String : AnyObject] ] {
                for jsonData in resultsArray {
                    let truckName: String = jsonData["truckName"] as! String
                    // let objectId: String = jsonData["objectId"] as! String
                    
                    trucks.append(
                        Truck(
                            truckName: truckName,
                            stops: [
                                Stop(
                                    name: stopsName[random() % stopsName.count],
                                    location: CLLocation(
                                        latitude: Double(random() % 600 + 1) / 1000000.0 ,
                                        longitude: Double(random() % 600 + 1) / 1000000.0
                                    )
                                )
                            ],
                            currentStopIndex: 0,
                            foodImageURL: foodImageUrls[random() % foodImageUrls.count]
                        )
                    )
                }
            }
            
            XCTAssert(trucks.count > 0, "Trucks array shouldn't be empty")
            expectation.fulfill()
        }
        
        loadTask.resume()
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}

//
//  TruckPersistentTests.swift
//  PingMyTruck
//
//  Created by Tan Ji Sheng on 02/12/2015.
//  Copyright © 2015 Asterisque Technology Malaysia Sdn. Bhd. All rights reserved.
//

import XCTest
@testable import PingMyTruck
import MapKit

class TruckPersistentTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //    func testSavingTruck() {
    //        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    //
    //        var stops: [Stop] = []
    //        let currentStop: Stop = Stop(name: "Cyberjaya", location: CLLocation(latitude: 5, longitude: 5))
    //        stops.append(Stop(name: "MaGIC",location: CLLocation(latitude: 1, longitude: 1)))
    //        stops.append(currentStop)
    //        stops.append(Stop(name: "Putrajaya", location: CLLocation(latitude: 3, longitude: 3)))
    //        let newTruck: Truck = Truck(truckName: "CendolTruck", stops: stops, currentStopIndex: 1, foodImageURL:NSURL(string: "https://www.usedvending.com/images/food-trucks-mobile-kitchens-for-sale/antique-food-truck-milk.jpg")! )
    //        XCTAssertTrue(newTruck.truckName == "CendolTruck" && newTruck.getCurrentStop().name == currentStop.name)
    //        //Create new Truck
    //        let truckData: NSData = NSKeyedArchiver.archivedDataWithRootObject(newTruck)
    //
    //        defaults.setObject(truckData, forKey: "testTruck")
    //        defaults.synchronize()
    //
    //        let readTruckData: NSData = defaults.objectForKey("testTruck") as! NSData
    //        let readTruck: Truck? = NSKeyedUnarchiver.unarchiveObjectWithData(readTruckData) as? Truck
    //        XCTAssertNotNil(readTruck, "Read back from saved data")
    //        XCTAssertEqual(readTruck!.truckName, newTruck.truckName, "Names should match")
    //    }
    
    func testWritingToFile(){
        var trucks = [Truck]()
        let foods:[String] = ["Keropok","Rojak","Durian","Cendol","Fruit"]
        let stopsName:[String] = ["Cyberjaya","Putrajaya","KLCC","IOI Mall","Menara A & B"]
        let foodImageUrls: [String] = ["https://system.qtixapp.com/images/prettyphoto/1.jpg","https://system.qtixapp.com/images/prettyphoto/2.jpg","https://system.qtixapp.com/images/prettyphoto/3.jpg","https://system.qtixapp.com/images/prettyphoto/4.jpg","https://www.usedvending.com/images/food-trucks-mobile-kitchens-for-sale/antique-food-truck-milk.jpg","https://bakerytrucksforsale.com/images/uploads/ford_walkin_baking_snack_truck_1.jpg"
        ]
        
        for _ in 1...5{
            trucks.append(
                Truck(
                    truckName: foods[random() % foods.count]+"Truck",
                    stops: [
                        Stop(
                            name: stopsName[random() % stopsName.count],
                            location: CLLocation(
                                latitude: Double(random() % 600 + 1) / 1000000.0 ,
                                longitude: Double(random() % 600 + 1) / 1000000.0
                            )
                        )
                    ],
                    currentStopIndex: 0,
                    foodImageURL: foodImageUrls[random() % foodImageUrls.count]
                )
            )
        }
        
        TruckLoader.sharedTruckLoader.saveTruckToFile(trucks)
    }
    
    func testReadFromFile() {
        let trucks: [Truck] = TruckLoader.sharedTruckLoader.readTruckFromFile()
        
        XCTAssert(trucks.count >= 5, "The truck should be five")
    }
    //    func testSavingTruckArray() {
    //        var trucks = [Truck]()
    //        let foods:[String] = ["Keropok","Rojak","Durian","Cendol","Fruit"]
    //        let stopsName:[String] = ["Cyberjaya","Putrajaya","KLCC","IOI Mall","Menara A & B"]
    //        let foodImageUrls: [NSURL] = [
    //            NSURL(string: "https://system.qtixapp.com/images/prettyphoto/1.jpg")!,
    //            NSURL(string: "https://system.qtixapp.com/images/prettyphoto/2.jpg")!,
    //            NSURL(string: "https://system.qtixapp.com/images/prettyphoto/3.jpg")!,
    //            NSURL(string: "https://system.qtixapp.com/images/prettyphoto/4.jpg")!,
    //            NSURL(string: "https://www.usedvending.com/images/food-trucks-mobile-kitchens-for-sale/antique-food-truck-milk.jpg")!,
    //            NSURL(string: "https://bakerytrucksforsale.com/images/uploads/ford_walkin_baking_snack_truck_1.jpg")!
    //        ]
    //
    //        for _ in 1...100{
    //            trucks.append(
    //                Truck(
    //                    truckName: foods[random() % foods.count]+"Truck",
    //                    stops: [
    //                        Stop(
    //                            name: stopsName[random() % stopsName.count],
    //                            location: CLLocation(
    //                                latitude: Double(random() % 600 + 1) / 1000000.0 ,
    //                                longitude: Double(random() % 600 + 1) / 1000000.0
    //                            )
    //                        )
    //                    ],
    //                    currentStopIndex: 0,
    //                    foodImageURL: foodImageUrls[random() % foodImageUrls.count]
    //                )
    //            )
    //        }
    //
    //        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    //        let truckData: NSData = NSKeyedArchiver.archivedDataWithRootObject(trucks)
    //        defaults.setObject(truckData, forKey: "SavedTrucks")
    //        defaults.synchronize()
    //
    //        if let trucksData :NSData = defaults.objectForKey("SavedTrucks") as? NSData {
    //
    //            if let readTrucks: [Truck]? = NSKeyedUnarchiver.unarchiveObjectWithData(trucksData) as? [Truck]
    //            {
    //                XCTAssert(readTrucks!.count > 0, "Saved list of trucks should contain some items")
    //                XCTAssert(readTrucks!.count == 100, "Saved list of trucks should contain some items")
    //            }
    //        } else {
    //            XCTAssert(false, "Unable to read list of trucks")
    //        }
    //
    //    }
    
    //    func testReadingArray() {
    //        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    //
    //        if let trucks = defaults.objectForKey("SavedTrucks") as? [Truck] {
    //            XCTAssert(trucks.count > 0, "Saved list of trucks should contain some items")
    //            XCTAssert(trucks.count == 100, "Saved list of trucks should contain some items")
    //        } else {
    //            XCTAssert(false, "Unable to read list of trucks")
    //        }
    //    }
    
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}